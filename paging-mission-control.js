const fs = require('fs');

// index of fields in the telemetry record
const TIME_IDX = 0;
const SATID_IDX = 1;
const REDHI_IDX = 2;
const YELHI_IDX = 3;
const YELLO_IDX = 4;
const REDLO_IDX = 5;
const RAW_IDX = 6;
const COMP_IDX = 7;

/**
 * Classify severity of reading given red and yellow bands
 * @param  {number} value - raw value of reading      
 * @param  {number} red_high    
 * @param  {number} yellow_high 
 * @param  {number} yellow_low  
 * @param  {number} red_low     
 * @return {string} severity classifier 
 */
function severity(value, red_high, yellow_high, yellow_low, red_low) {

  if (value >= red_high) {
    return 'RED HIGH';
  }

  if (value >= yellow_high) {
    return 'YELLOW HIGH';
  }

  if (value <= red_low) {
    return 'RED LOW';
  }    

  if (value <= yellow_low) {
    return 'YELLOW LOW';
  }

  if (value > yellow_low && value < yellow_high) {
    return 'OK';
  }

  return 'classification error';
}

/**
 * Fix formatting of timestamp so that it can be converted to UTC
 * @param  {string} timestamp Telemetry timestamp
 * @return {string} formatted timestamp
 */
function fixDate(timestamp) {
  return `${timestamp.substring(0, 4)}-${timestamp.substring(4, 6)}-${timestamp.substring(6, 8)}T${timestamp.substring(9)}Z`;
}

/**
 * Check if the satellite's warning has occured multiple times within a sample window of the temetry feed and report a violation if occurances reach the limit. 
 * @param  {object}  sample   telemetry alert message for satellite
 * @param  {array}   samples  array of telemetry alert messages to check sample against. Assuming time ordered with last sample last.
 * @param  {number}  windowMs millisecond sample window to compare sample for
 * @param  {number}  limit    occurances of satellite severity warning in data that is a violation
 * @return {boolean}          true sample is in violation - the satellite warning a 'limit' number of times within the sample window
 */
function isViolation(sample, samples, windowMs, limit) {
  
  let count = 1;
  
  // go through samples to look for repeats of this satellite's warning.
  for (let i = samples.length - 1; i >= 0; i--) {
    if (sample.satelliteId === samples[i].satelliteId && sample.severity === samples[i].severity && msDiff(sample.timestamp, samples[i].timestamp) < windowMs) {
      count++;
      if (count === limit) {
        return true;
      }
    } 
  }

  return false
}

/**
 * Calculate difference in milliseconds between two UTC timestample
 * @param  {string} time1 UTC string of time1
 * @param  {string} time2 UTC string of time2
 * @return {number}       difference in milliseconds between time1 and time2
 */
function msDiff(time1, time2) {
  const t1 = new Date(time1).getTime();
  const t2 = new Date(time2).getTime();

  return t1 - t2;
}


/**********************
  Processing of telemetry data
 *********************/
// read telemetry file into a double array of strings split by the pipe character '|'
const strArr = fs.readFileSync('sample-data.txt').toString().split('\n').map(s => { return s.split('|')});

// map array of telemetry records into a JSON array of telemetry alerts. 
const jsonAlertArr = strArr.map(data => {
  return {
    "satelliteId": parseInt(data[SATID_IDX], 10), // format as number
    "severity": severity(parseInt(data[RAW_IDX], 10), parseInt(data[REDHI_IDX], 10), parseInt(data[YELHI_IDX], 10), parseInt(data[YELLO_IDX], 10), parseInt(data[REDLO_IDX], 10)), // severity classification
    "component": data[COMP_IDX],
    "timestamp": fixDate(data[TIME_IDX]), // convert string to proper date time format
  }
});

/**********************
  Generate alerts for sample violations
 *********************/
// filter alerts for use in check against violation criteria: Red Low battery voltage readings and Red High Red themostat readings
const criticalAlerts = jsonAlertArr.filter(r => {return r.severity === 'RED HIGH' && r.component === 'TSTAT' || r.severity === 'RED LOW' && r.component === 'BATT'});

// go through critical alerts to check for samples that are in violation of the conditions
const REPEATS = 3; // 
const WINDOW = 5 * 60 * 1000; // look 5 minutes back in feed from sample's time for violations
const violations = [];
while (criticalAlerts.length > REPEATS) {
  const s = criticalAlerts.pop();
  if (isViolation(s, criticalAlerts, WINDOW, REPEATS)) {
    violations.push(s);
  }    
}

// output alerts to console
console.log(violations);

// console output. Note: the violation timestamp is of the sample that triggered the condition. The sample output in the challenge description provides the timestamp of the earliest sample of the alert - which does not make sense. 
// [
//   {
//     satelliteId: 1000,
//     severity: 'RED LOW',
//     component: 'BATT',
//     timestamp: '2018-01-01T23:04:11.531Z'
//   },
//   {
//     satelliteId: 1000,
//     severity: 'RED HIGH',
//     component: 'TSTAT',
//     timestamp: '2018-01-01T23:03:05.009Z'
//   }
// ]
